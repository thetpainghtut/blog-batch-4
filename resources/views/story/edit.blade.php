@extends('template')

@section('content')
	
	<div class="container">
    	<div class="row justify-content-center my-3">
        	<div class="col-md-8">
        		<h2 class="mb-2">Edit Form.</h2>
				<form method="post" action="{{route('newstory.update',$post->id)}}" enctype="multipart/form-data">
				<input name="_method" type="hidden" value="PUT">
					@csrf
			  	<div class="form-group">
			    	<label for="formGroupExampleInput">Categories</label>
			    	<select class="form-control" name="category">
			    		@foreach($categories as $category)
			    		<option value="{{$category->id}}" 
			    			@if($category->id == $post->category_id)
			    			{{'selected'}}
			    			@endif>{{$category->name}}</option>
			    		@endforeach
			    	</select>
			  	</div>
			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Title</label>
			    	<input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="formGroupExampleInput2" placeholder="Post Title" value="{{$post->title}}">

@if ($errors->has('title'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('title') }}</strong>
    </span>
@endif
			  	</div>

			  	<div class="form-group">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Old Image</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">New Image</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active my-3" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<img src="{{asset($post->image)}}" class="img-fluid w-25">
  	<input type="hidden" name="oldimage" value="{{$post->image}}">
  </div>
  <div class="tab-pane fade my-3" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  	<label for="formGroupExampleInput2">Photo</label>
			    	<input type="file" class="form-control-file {{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" id="formGroupExampleInput2">
@if ($errors->has('photo'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('photo') }}</strong>
    </span>
@endif

  </div>
</div>
			  	</div>

			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Body</label>
			    	<textarea class="form-control" placeholder="Another input" name="body" id="summernote">{{$post->body}}</textarea>
			  	</div>

			  	<div class="form-group">
			    	<input type="submit" class="btn btn-success" value="Save">
			  	</div>

				</form>
			</div>
		</div>
	</div>
@endsection