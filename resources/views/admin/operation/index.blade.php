@extends('template')

@section('content')
<div class="container">
    <div class="row justify-content-center my-5">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Testing Operations
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="{{route('admin.sendemail')}}">
                        @csrf
                        <div class="form-group">
                            <label>Email Address You want to send!</label>
                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email...">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="submit" name="btn" class="btn btn-primary" value="Send Email">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        
    </script>
@endsection