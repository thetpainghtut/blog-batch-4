<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<title>Mail Testing Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<div class="container" style="max-width: 800px;">
		<div class="row my-3">
			<div class="col-md-12">
				<h3>Bootcamp MMIT | <small>Daily Digest</small></h3>
				<hr>
				<small>Stories for You</small>
			</div>
		</div>
		<div class="row justify-content-center">
	        <div class="col-md-8">
				@foreach($posts as $post)
				<a href="{{route('newstory.show',$post->id)}}" target="_blank">
	            <div class="card mb-3">
					<div class="row no-gutters">
					    <div class="col-md-4">
					      <img src="{{$post->image}}" class="card-img" alt="...">
					    </div>
					    <div class="col-md-8">
					      <div class="card-body">
					        <h5 class="card-title text-dark">{{$post->title}}</h5>
					        <p class="card-text text-dark">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
					        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
					      </div>
					    </div>
					</div>
				</div>
				</a>
				@endforeach
	        </div>
	    </div>
	    
	    <div class="row my-3 bg-light">
	    	<div class="col-md-12 text-center">
	    		<span class="mr-1">Thanks,</span>
				<a href="{{asset('/')}}" class="btn btn-link">Bootcamp MMIT</a>
	    	</div>
	    </div>
	</div>
</body>
</html>