@extends('template')

@section('content')
	
	<div class="container">
    	<div class="row justify-content-center my-3">
        	<div class="col-md-8">
        		<h2 class="mb-2">New Story Form.</h2>
				<form method="post" action="{{route('newstory.store')}}" enctype="multipart/form-data">
					@csrf
			  	<div class="form-group">
			    	<label for="formGroupExampleInput">Categories</label>
			    	<select class="form-control" name="category">
			    		@foreach($categories as $category)
			    		<option value="{{$category->id}}">{{$category->name}}</option>
			    		@endforeach
			    	</select>
			  	</div>
			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Title</label>
			    	<input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="formGroupExampleInput2" placeholder="Post Title">

@if ($errors->has('title'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('title') }}</strong>
    </span>
@endif
			  	</div>

			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Photo</label>
			    	<input type="file" class="form-control-file {{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" id="formGroupExampleInput2">
@if ($errors->has('photo'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('photo') }}</strong>
    </span>
@endif
			  	</div>

			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Body</label>
			    	<textarea class="form-control" placeholder="Another input" name="body" id="summernote"></textarea>
			  	</div>

			  	<div class="form-group">
			    	<input type="submit" class="btn btn-success" value="Save">
			  	</div>

				</form>
			</div>
		</div>
	</div>
@endsection