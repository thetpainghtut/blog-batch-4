<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestingMail extends Mailable
{
    use Queueable, SerializesModels;
    public $posts,$subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($posts,$subject)
    {
        //
        $this->posts = $posts;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->markdown('emails.testing.mymail')
                    ->with('posts', $this->posts);
    }
}
