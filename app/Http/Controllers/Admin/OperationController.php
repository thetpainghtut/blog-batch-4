<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestingMail;

class OperationController extends Controller
{
    //
    public function __construct()
    {
        // User must be a admin and authnenticated
        $this->middleware('is_admin');
    }

    public function index($value='')
    {
    	# code...
    	return view('admin.operation.index');
    }

    public function sendemail(Request $request)
    {
        // Validate 
        $request->validate([
            'email' => 'required|email',
        ]);

    	# code...
    	$posts = Post::all();

        $user = request('email');
        $subject = 'This is a Subject';
        Mail::to($user)->send(new TestingMail($posts,$subject));

        return redirect()->back();
    }
}
