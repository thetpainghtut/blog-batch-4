@extends('template')
@section('content')
	<nav class="navbar navbar-expand navbar-light bg-white">
		<div class="container">
			<a class="navbar-brand mx-3" href="#"><small>All Articles</small></a>

			<div class="collapse navbar-collapse">
			    <ul class="navbar-nav ml-auto">
				    <li class="nav-item dropdown mx-2">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Categories
				        </a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	@foreach($categories as $category)
  	<a class="dropdown-item" href="/?category_id={{$category->id}}">{{$category->name}}</a>
  	@endforeach
</div>
				    </li>
			    </ul>
			    <form class="form-inline my-2 my-lg-0">
			      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			    </form>
			</div>
		</div>
	</nav>

	<section class="py-5 bg-mylight">
	  <div class="container">
	    <div class="row">
	    	@foreach($posts as $post)
	    	<div class="col-md-4">
    			<div class="card" style="border-radius: 1rem;">
    				<a href="{{route('newstory.show',$post->id)}}" class="mb-2 d-block" style="cursor: pointer !important;text-decoration: none;">
					    <img class="card-img-top" src="{{ asset($post->image) }}" alt="Card image cap">
					  	<div class="card-img-overlay">
							<span class="card-title">{{$post->category->name}}</span>
						</div>
					    <div class="card-body">
						  	<p class="text-dark">{{$post->user->name}}</p>
							<p>{{$post->title}}</p>
							
						    <p class="lead small mb-0 text-secondary">{{ $post->created_at->toFormattedDateString() }}</p>
					    </div>
					</a>
				</div>
    		</div>
    		@endforeach
	    </div>
	  </div>
	</section>

	<section class=" py-3 bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- <nav aria-label="Page navigation example">
					  <ul class="pagination justify-content-end mb-0">
					    <li class="page-item disabled">
					      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
					    </li>
					    <li class="page-item"><a class="page-link" href="#">1</a></li>
					    <li class="page-item"><a class="page-link" href="#">2</a></li>
					    <li class="page-item"><a class="page-link" href="#">3</a></li>
					    <li class="page-item">
					      <a class="page-link" href="#">Next</a>
					    </li>
					  </ul>
					</nav> -->
					{{ $posts->links() }}
				</div>
				
			</div>
		</div>
	</section>
@endsection