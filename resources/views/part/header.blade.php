<nav class="navbar navbar-expand-lg navbar-light bg-mylight sticky-top shadow">
		<div class="container">
			<a class="navbar-brand" href="/">
			    <img src="{{ asset('template/images/logo.svg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
			    Bootcamp <small>MMIT</small>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
			      <li class="nav-item active">
			        <a class="nav-link mx-2" href="#">About <span class="sr-only">(current)</span></a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link mx-2" href="#">Features</a>
			      </li>
			   <!-- If Admin -->
			   	@if(Auth::check() && Auth::user()->role == 'admin')
			    	<li class="nav-item active">
			        	<a class="nav-link mx-2" href="{{route('category.index')}}">Categories<span class="sr-only">(current)</span></a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link mx-2" href="{{route('post.index')}}">Posts</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link mx-2" href="{{route('admin.operation')}}">Operations</a>
			      	</li>
			    @endif

			    @guest
			    <li class="nav-item">
			        <a class="nav-link mx-2" href="{{route('login')}}" tabindex="-1" aria-disabled="true">Login</a>
			    </li>
			    <li class="nav-item">
			        <a class="btn btn-outline-success btn-sm my-2 ml-2" href="{{route('register')}}" tabindex="-1" aria-disabled="true">Sign Up</a>
			    </li>
			    @else
			    <li class="nav-item dropdown mx-2">
			    	@if(Auth::user()->profile == 'profilepic')
				    <img src="{{asset('template/images/profile.png')}}"  width="30" class="img-fluid dropdown-toggle d-block my-2 ml-2 rounded-circle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" style="cursor: pointer;">
				    @else
				    <img src="{{asset(Auth::user()->profile)}}"  width="30" class="img-fluid dropdown-toggle d-block my-2 ml-2 rounded-circle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" style="cursor: pointer;">
				    @endif
				    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				    	<a class="dropdown-item" href="#">{{Auth::user()->name}}</a>
				          <a class="dropdown-item" href="{{route('profile.edit',Auth::user()->id)}}">Profile</a>
				          <a class="dropdown-item" href="{{route('newstory.create')}}">New Story</a>
				          <div class="dropdown-divider"></div>
				          <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
				    </div>
				</li>
				@endguest
			    </ul>
			</div>
		</div>
	</nav>