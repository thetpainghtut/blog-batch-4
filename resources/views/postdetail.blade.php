@extends('template')
@section('content')
	<section class="py-5 bg-mylight">
	  <div class="container">
	    <div class="row">
	    	<div class="col-md-6">
    			<h2 class="mb-5">{{$post->title}}</h2>

    			<div class="media my-5">
    				@if($post->user->profile == 'profilepic')
					  <img src="{{asset('template/images/profile.png')}}" class="mr-3 rounded-circle" width="50" alt="...">
					@else
					  <img src="{{asset($post->user->profile)}}" class="mr-3 rounded-circle" width="50" alt="...">
					@endif
				  <div class="media-body">
				    <h5 class="mt-0">{{$post->user->name}}</h5>
				    <span>{{$post->created_at->toFormattedDateString()}}</span>
				  </div>
				</div>
				<button class="btn btn-lg btn-outline-success" style="border-radius: 2rem !important;"><i class="fas fa-tags"></i> <span class="pl-2"><a href="/?category_id={{$post->category->id}}" style="text-decoration: none;color: inherit;">{{$post->category->name}}</a></span></button>

				@if(Auth::check() && ($post->user_id == Auth::user()->id))
				<div class="form-group mt-3">
					<a href="{{route('newstory.edit',$post->id)}}" class="btn btn-warning">Edit</a>

				<form method="post" action="{{route('newstory.destroy',$post->id)}}" class="d-inline-block">
					<input type="hidden" name="_method" value="DELETE">
					@csrf
					<input type="submit" class="btn btn-danger" value="Delete">
				</form>
				</div>
					
				@endif
    		</div>
    		<div class="col-md-6 py-3">
    			<img src="{{asset($post->image)}}" class="img-fluid">
    		</div>
	    </div>
	  </div>
	</section>

	<section class="py-5 bg-white">
	  <div class="container">
	    <div class="row my-5">
	    	<div class="col-md-8 offset-md-2">
	    		<p class="lead">
	    			{!!$post->body!!}
	    		</p>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-8 offset-md-2">
	    		<!-- <form method="post" action="{{route('comment.store')}}"> -->
	    			@csrf
	    			<input type="hidden" name="post_id" value="{{$post->id}}" id="post_id">
	    			<div class="form-group">
	    				<textarea class="form-control {{ $errors->has('comment') ? ' is-invalid' : '' }}" rows="5" name="comment" placeholder="Write Comments..." id="comment"></textarea>
@if ($errors->has('comment'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('comment') }}</strong>
    </span>
@endif
	    			</div>

	    			<!-- <input type="submit" name="btn" value="Post" class="btn btn-success"> -->
	    			<button class="btn btn-success" id="givecomment">Post</button>
	    		<!-- </form> -->
	    	</div>

	    	<div class="col-md-8 offset-md-2 my-5">
	    		<div class="card my-3 shadow">
				  <h5 class="card-header">Comments..</h5>
				  <div class="card-body" >
				  	<div id="comments"></div>

					

  				  </div>
				</div>
	    	</div>
	    </div>
	  </div>
	</section>
@endsection
@section('script')
	<script type="text/javascript">
		comments();

		$('#givecomment').click(function (e) {
			// body...
			e.preventDefault();
			var comment = $('#comment').val();
			var post_id = $('#post_id').val();

			// alert(comment + post_id);

			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

			$.post('/comment',
                {post_id:post_id,comment:comment},
                function (response) {
                // alert(response.success);
               comments();
            });
		});

		function comments() {

			var post_id = $('#post_id').val();

			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

			$.get('/comment',
                {post_id:post_id},
                function (response) {
                console.log(response.data);
                var html = '';
                var i =0;
                var data = response.data.slice(0,3);
                $.each(data,function (i,v) {
                	// console.log(v.body);
                	html += '<div class="media mb-5">';

                	if(v.user.profile == 'profilepic'){
                		html += '<img src="{{asset('template/images/profile.png')}}" class="mr-3 img-fluid rounded-circle" width="30" alt="...">';
                	}else{
                		html += '<img src="'+v.user.profile+'" class="mr-3 img-fluid rounded-circle" width="30" alt="...">';
                	}
                	

                		html += '<div class="media-body">'+
					    '<h5 class="mt-0">'+v.user.name+
					    '<small class="float-right">'+v.created_at+'</small>'+
					    '</h5>'+v.body+'</div>'+
					'</div>';
					i++;

					
					
                });
                if (response.data.length >3 && i != response.data.length) {
						html += '<p style="cursor: pointer;" id="morecomments">More Comments...</p>';
					}
                $('#comments').html(html);
            });
		}


		$('#comments').on('#morecomments','click',function (e) {
			e.preventDefault();
			
		})
	</script>
@endsection