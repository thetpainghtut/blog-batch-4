@extends('template')

@section('content')
	
	<div class="container">
    	<div class="row justify-content-center my-5">
        	<div class="col-md-8">
        		<h2 class="mb-2">Edit Profile Form</h2>
				<form method="post" action="{{route('profile.update',$user->id)}}" enctype="multipart/form-data">
				<input name="_method" type="hidden" value="PUT">
					@csrf

				<div class="form-group">
					<div class="form-file">
	                    <input type="file" class="inputfile" name="your_picture" id="your_picture"  onchange="readURL(this);" data-multiple-caption="{count} files selected" multiple />
	                    <label for="your_picture">
	                        <figure>
	                        	@if($user->profile == 'profilepic')
	                            <img src="{{ asset('template/images/profile.png') }}" alt="" class="your_picture_image">
	                            @else
	                            <img src="{{ asset($user->profile) }}" alt="" class="your_picture_image">
	                            @endif
	                            <input type="hidden" name="oldprofile" value="{{$user->profile}}">
	                        </figure>
	                        <span class="file-button">choose profile</span>
	                    </label>
	                </div>
	                @if ($errors->has('your_picture'))
    <span class="invalid-feedback" role="alert" style="display: block;">
        <strong>{{ $errors->first('your_picture') }}</strong>
    </span>
@endif
				</div>
			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Name</label>
			    	<input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="formGroupExampleInput2" placeholder="Post Title" value="{{$user->name}}">

					@if ($errors->has('name'))
					    <span class="invalid-feedback" role="alert">
					        <strong>{{ $errors->first('name') }}</strong>
					    </span>
					@endif
			  	</div>

			  	<div class="form-group">
			    	<label for="formGroupExampleInput2">Email</label>
			    	<input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="formGroupExampleInput2" placeholder="Email" value="{{$user->email}}">

					@if ($errors->has('email'))
					    <span class="invalid-feedback" role="alert">
					        <strong>{{ $errors->first('email') }}</strong>
					    </span>
					@endif
			  	</div>

			  	<div class="form-group">
			    	<input type="submit" class="btn btn-success" value="Update">
			  	</div>

				</form>
			</div>
		</div>
	</div>
@endsection