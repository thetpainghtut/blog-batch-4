<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('template');
// });

Route::get('/','MainController@index');

Route::get('/about','MainController@about');
Route::get('/features','MainController@features');

// For Profile
Route::resource('/profile','ProfileController');

// For New Story
Route::resource('/newstory','PostController');

// For Comments
Route::resource('/comment','CommentController');

// For Categories (only Admin)
Route::resource('/admin/category','Admin\CategoryController');

// For Posts (only Admin)
Route::resource('/admin/post','Admin\PostController');

// For Operations (only Admin)
// For Testing (e.g -> email, ..)
Route::get('/admin/operation','Admin\OperationController@index')->name('admin.operation');

Route::post('/admin/sendemail','Admin\OperationController@sendemail')->name('admin.sendemail');

Auth::routes();

Route::get('/home', 'HomeController@index')
		->middleware('is_admin')
		->name('home');
