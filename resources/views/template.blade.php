<!DOCTYPE html>
<html>
<head>
	<title>MMIT Bootcamp</title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('template/images/favicon.ico')}}" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('template/fontawesome/css/all.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('template/css/custom.css') }}">

    <!-- For summernote -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
    
    <!-- Jquery -->
    <script type="text/javascript" src="{{ asset('template/js/jquery.min.js') }}"></script>
    
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('template/js/bootstrap.bundle.min.js') }}"></script>

    <!-- For summernote -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
</head>
<body>
	
	<!-- Header -->
	@include('part.header')

	@yield('content')

	<!-- Footer -->
	@include('part.footer')

    <script>
      $('#summernote').summernote({
        placeholder: 'Blog Post..',
        tabsize: 2,
        height: 400
      });

      // For Profile Upload
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.your_picture_image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    @yield('script')
</body>
</html>