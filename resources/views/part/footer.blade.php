<footer class="py-5 bg-dark text-white">
	  <div class="container">
	    <div class="row">
	    	<div class="col-md-4">
    			<ul class="list-unstyled">
				  <li>Facilisis in pretium nisl aliquet</li>
				  <li>Nulla volutpat aliquam velit
				    <ul>
				      <li>Phasellus iaculis neque</li>
				      <li>Purus sodales ultricies</li>
				      <li>Vestibulum laoreet porttitor sem</li>
				      <li>Ac tristique libero volutpat at</li>
				    </ul>
				  </li>
				</ul>
    		</div>
	    </div>
	  </div>
	</footer>